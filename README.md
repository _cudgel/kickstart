> # 26.1. What are Kickstart Installations?
> 
> Kickstart installations offer a means to automate the installation process, either partially or fully. Kickstart files contain answers to all questions normally asked by the installation program, such as what time zone you want the system to use, how the drives should be partitioned, or which packages should be installed. Providing a prepared Kickstart file when the installation begins therefore allows you to perform the installation automatically, without need for any intervention from the user. This is especially useful when deploying Red Hat Enterprise Linux on a large number of systems at once.
> 
> Kickstart files can be kept on a single server system and read by individual computers during the installation. This installation method can support the use of a single Kickstart file to install Red Hat Enterprise Linux on multiple machines, making it ideal for network and system administrators.
> 
> All Kickstart scripts and the log files of their execution are stored in the /tmp directory to assist with debugging installation failures. 

From the [RHEL7 Installation Guide, Chapter 26](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/chap-kickstart-installations)

## Quick and Dirty guide to kickstart

If you've ever installed RHEL, CentOS, or their ilk, you've used kickstart. You'll always find `/root/anaconda-ks.cfg` after the install; this file contains the selections you made in the interactive installer. You can use it to recreate the system, should the need arise. You can supply your own `ks.cfg` to the Anaconda installer, saved afterwards as `/root/original-ks.cfg`, to fully automate installation.

This guide will show a few methods to provide your own `ks.cfg` file to the installer, as well as provide some sample `ks.cfg` files and/or snippets to perform common tasks.

### The easy route

Let's say you've made a bootable USB drive with the CentOS-7-x86_64-Minimal ISO.When you boot your USB drive and see the option `Install CentOS 7`, hit tab. You can edit the boot string, providing additional configuration.

Here's a sample of the default boot string:
```
> vmlinuz initrd=initrd.img inst.stage.2=hd:LABEL=CentOS\x207\x20x86_64 quiet
```

Here's how to supply a kickstart file via URL:
```
> vmlinuz initrd=initrd.img inst.stage.2=hd:LABEL=CentOS\x207\x20x86_64 quiet inst.ks=http://10.1.2.3/my-ks.cfg ip=em1:dhcp
```

Note the inclusion of two parameters: `inst.ks=` and `ip=`. Of course, this method requires that the URL you're attempting to use is accessible to the host. Additional information can be found in [Chaper 22.1 of the RHEL7 Installation Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/chap-anaconda-boot-options#sect-boot-options-installer).


### The complicated but still pretty easy route

Perhaps you're in a situation where configuring the network interfaces at install time is impossible, but you still want to use Kickstart. Check out the `iso` directory to learn [How to make a copy of a CentOS 7 ISO with your `ks.cfg` file included](iso/).

## Notes on choice of .iso distribution

If you look at the available .iso files to download, you're faced with a choice. The [different distributions]() fit different roles, and it's wise to choose one that fits your situation best. For these examples, I'll be using the NetInstall distribution. Not only is it the smallest .iso, it assumes network connectivity and defaults to what is the most flexible install method. One does not simply always NetInstall though -- make sure you download Minimal, DVD, or Everything distributions if network connectivity is an issue.

Whatever file from the iso directory you choose to download, make sure to verify it's integrity. It's easy to overlook this step, but it's an important one. The TL;DR on this is to grab the `sha256sum.txt.asc` file from the iso directory and use it to verify the checksum of the .iso. The [complete instructions](https://wiki.centos.org/Download/Verify) are helpful if you've never done this step before -- and you should definitely read them and verify the cryptographic signature of the `sha256sum.txt.asc` file. Below is the bare minimum check you should do; managing GPG is beyond the scope of this document.

```
#!console
$ ls -l *.iso *.asc
-rw-rw-r--. 1 jim jim 531628032 Feb 12 13:19 CentOS-7-x86_64-NetInstall-1810.iso
-rw-rw-r--. 1 jim jim      1458 Feb 12 13:20 sha256sum.txt.asc
$ sha256sum -c sha256sum.txt.asc 2>&1 | grep OK
CentOS-7-x86_64-NetInstall-1810.iso: OK
```

## Kickstart Files

In the `ks` directory, you'll find several ks.cfg files useful in different circumstances. Generally, the order in these files doesn't matter. Have a look at `/root/anaconda-ks.cfg` and you'll see the elements in the file ordered alphabetically. I prefer something a bit more readable, and break up the main section of the file into logical chunks offset by comments.

### Base Install

The base install is barebones, mimimal package set, and dead simple. Please give the following file a thorough read:

[ks/cdrom-base-ks.cfg](ks/cdrom-base-ks.cfg)

### Adding repos at install time

By default, the installer knows about one yum repo: Base. Once the system is installed and turned over to user control, it's fairly common to run `yum -y update` to apply any outstanding updates to the system since the installer was created. You can install a fully updated system simply by including the Updates repo in the Kickstart file -- `yum` will resolve the latest versions of packages from all available repos.

The Updates repo isn't included on the install media, so your installer will need to have network connectivity for this to work. Presuming you've got that worked out, all you need to do is add the repo to the `ks.cfg` file:
```
repo --name=updates --mirrorlist=http://mirrorlist.centos.org/?release=7&arch=x86_64&repo=updates
```

### Setting up disks & filesystems

Our `base-ks.cfg` file contains the default partition layout, using LVM, as follows:
```
# Disk Configuration
ignoredisk --only-use=sda
clearpart --all --initlabel --drives=sda
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
autopart --type=lvm
```
This configuration:

 - limits itself to the device `sda`'
 - wipes the `sda` partition table
 - creates the partitions `/dev/sda1` and `/dev/sda2`
 - installs a MBR bootblock in `/dev/sda1`
 - creates `/boot`, an XFS filesystem mounted from `/dev/sda1`
 - creates a LVM phyical volume from `/dev/sda2`
 - adds `/dev/sda2` to the LVM volume group `centos`
 - creates the LVM logical volume `swap`, sized at double system memory, from the `centos` volume group
 - creates the LVM logical volume `root`, sized to fill the remaining disk space, from the `centos` volume group
 - adds the `swap` volume as swap space
 - create `/`, an XFS filesystem mounted from the LVM logical volume `root`

For most cases, `autopart` is a reasonable choice. If you have more specific needs, be aware of the following note from [Kickstart Syntax Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax):

>  The `autopart` option cannot be used together with the `part`/`partition`, `raid`, `logvol`, or `volgroup` options in the same Kickstart file. 

If you have specific partitioning requirements, I suggest giving all the relevant sections of the [Kickstart Syntax Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax) a thorough read. There are enough options accomplish whatever your goal happens to be.

If you're looking for quick & dirty, you can always run shell commands in `%post` -- but you can do it just as easily using the provided commands. Here's a snippet from [ks/netinstall-customdisks-ks.cfg](ks/netinstall-customdisks-ks.cfg), setting up both `/dev/sda` and `/dev/sdb`:

```
# Disk Configuration
ignoredisk --only-use=sda,sdb
clearpart --all --initlabel --drives=sda,sdb
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=sda
part /boot --ondisk=sda --asprimary --size=1000 --type=xfs
part pv.01 --ondisk=sda --asprimary --size=1 --grow
volgroup centos pv.01
logvol swap --name=swap --vgname=centos --hibernation
logvol / --name=root --vgname=centos --size=1 --grow
part /data1 --ondisk=sdb --asprimary --size=1 --grow
```

### Setting up networking

When setting up networking, it's useful to be familiar with [Chapter 11: Consistent Network Device Naming](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/ch-consistent_network_device_naming) of the [RHEL 7 Networking Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/).

The following is sufficient to enable a network-based install from `em1` on a Dell server:

```
network --device=em1 --activate --bootproto=dhcp --nameserver=64.202.97.1,64.202.97.2
```

Much like partitioning, configuring networking is an advanced topic, best covered by reading the syntax guide. 

#### Configuring hostnames and name resolution
```
network --hostname testserver.ord6.servercentral.net --nameserver=64.202.97.1,64.202.97.2
```

#### Creating static IP assignments
```
network --device=em1 --activate --bootproto=static --onboot=true ip=10.1.0.10 --netmask=255.255.255.0 --gateway=10.1.0.1
```

#### Creating bond interfaces

Note that no configuration of the physical interfaces is necessary. The installer will automagically do the right thing for both `p3p1` and `p3p2`. Here's a snippet from [ks/netinstall-bonding-ks.cfg](ks/netinstall-bonding-ks.cfg):
```
network --device=bond0 --no-activate --bootproto=static --onboot=true ip=10.0.0.10 --netmask=255.255.255.0 --gateway=10.0.0.1 --bondslaves=p3p1,p3p2 --bondopts=mode=802.3ad,miimon=100
```

### Authentication and password hashes

The default authentication scheme uses local users from `/etc/passwd` with shadow passwords. The shadow passwords are hashed with the sha512 algorithm. Shadow hashes are salted one-way hashes which makes them mostly safe to include in your `ks.cfg` files. Creating them is straightfoward:
```
#!console
$ python -c 'import crypt,getpass;pw=getpass.getpass();print(crypt.crypt(pw) if (pw==getpass.getpass("Confirm: ")) else exit())'
Password:
Confirm:
$6$oT2KvgZtckpdxP/M$/3B8qibkoQIHQ6OqjrO79snvI8HPKQtIbuL0ZQuJtbc5sO3ykz4mGdiSu8dF5.mISOEk6nVD5RxsqA.i6vw/8/
```
Some notes:

 - The python script to generate these hashes includes a random salt. That means re-running this command and providing the same input password should always generate different output.
 - If you intend to use one of these hashes as an argument to `useradd -p` in `%post`, remember to use single-ticks `'` around the string lest ye be beset by unintended shell interpolation.
 - Don't be lazy and use `--plaintext` and be wary of the fact that these hashes will be left behind after the install in `/root/anaconda-ks.cfg` and `/root/original-ks.cfg`. If you do use `--plaintext` expect the aforementioned files to contain plaintext passwords for users created at OS install time. This is a mind-numbingly stupid thing to do.

The `auth` keyword in `ks.cfg` files is one of the more interesting commands. With it, you can setup your system at install time to use Kerberos, LDAP, or Active Directory authentication in addition to local users.

### Package selection: `%packages`

The `%packages` section of a `ks.cfg` file is a list of desired packages, groups, or environments, one per line, ending with `%end`. The default set of packages is:

```
%packages
@core
kexec-tools
chrony
%end
```

To specify an environment, prepend the selection with `@^`, as in:
```
%packages
@^Infrastructure Server
%end
```

To specify a group, prepend the selection with `@`, as in:
```
%packages
@X Window System
%end
```

To determine available environments and groups, use the `yum group list` command:

```
#!console
$ yum group list
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.genesisadaptive.com
 * epel: mirror.steadfastnet.com
 * extras: mirror.genesisadaptive.com
 * updates: mirror.genesisadaptive.com
Available Environment Groups:
   Minimal Install
   Compute Node
   Infrastructure Server
   File and Print Server
   Cinnamon Desktop
   MATE Desktop
   Basic Web Server
   Virtualization Host
   Server with GUI
   GNOME Desktop
   KDE Plasma Workspaces
   Development and Creative Workstation
Installed Groups:
   Development Tools
Available Groups:
   Cinnamon
   Compatibility Libraries
   Console Internet Tools
   Educational Software
   Electronic Lab
   Fedora Packager
   General Purpose Desktop
   Graphical Administration Tools
   Haskell
   Legacy UNIX Compatibility
   MATE
   Milkymist
   Scientific Support
   Security Tools
   Smart Card Support
   System Administration Tools
   System Management
   TurboGears application framework
   Xfce
Done
```

Any package available in any of the configured `yum` repositories can be installed via this method. Note that a missing package can bring your fully automatic install to a screeching halt, unless you specify `%packages --ignoremissing`. See the syntax guide for more useful options.

### Post installation scripts `%post`

The `%post` section allows you, by default, to run commands in a `chroot` environment on your newly installed system. By default, `%post` uses `/bin/bash` as it's interpreter. If you prefer something else, use the `--interpreter` flag. Like `%packages`, the `%post` is terminated with `%end`.

The `%post` section of the installer is where local customizations often take place. From here, you might invoke your configuration management tool of choice, such as `ansible` or `chef`. Perhaps you need to install several users or groups, or ... well frankly whatever you can imagine.

From having done literally thousands of these installs, my advice is to keep it light. Do the bare minimum you need to get the job done. Don't overcomplicate `%post` -- it's easy to do.

## Additional Links

 - [Product Documentation for Red Hat Enterprise Linux 7](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/)
 - [RHEL 7 Installation Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/)
 - [RHEL 7 Kickstart Syntax Reference](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax)
 - [CentOS Mirror List](https://www.centos.org/download/mirrors/)

