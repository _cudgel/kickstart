### The complicated but still pretty easy way

Perhaps you're in a situation where configuring the network interfaces at install time is impossible, but you still want to use Kickstart. This section will detail how to make a copy of a CentOS 7 ISO with your `ks.cfg` file included.

There are some prerequisites for doing this:

 - A pre-existing CentOS 7 system, with
   - `root` privileges, for mounting a .iso on a loopback device
   - the following packages:
     - `curl`
     - `rsync`
     - `pykickstart`
     - `genisoimage`
     - `isomd5sum`
   - network connectivity to download a .iso file

Contained in this directory, you'll also find the [`create-iso.sh`](iso/create-iso.sh) script and the associated [`isolinux.cfg.patch`](iso/isolinux.cfg.patch) files. Together, these provide a little automation for the steps detailed below. I'd say it's a *Use at your own risk* sort of tool; its quick and dirty with an emphasis on the dirty. :D

If you find yourself wanting to patch the installer menu yourself, you can do something like the following, after having copied the contents of a distribution .iso to local disk:
```
#!console
# pwd
/root
# cd OEMDRV/isolinux
# cp isolinux.cfg isolinux.mine
# vi isolinux.mine
# diff -u isolinux.cfg isolinux.mine > ../../isolinux.cfg.patch
```

Patch files used by the `create-iso.sh` script are just [unified diff](https://en.wikipedia.org/wiki/Diff) files, so if you have a preferred way to diff, do that.

#### Step 1: Create some working directories
```
#!console
# pwd
/root
# mkdir work
# cd work
# pwd
/root/work
# mkdir mnt OEMDRV
```

#### Step 2: Obtain & verify the .iso

Use `curl` to download an installer:
```
#!console
# pwd
/root
# curl -L -O http://mirror.team-cymru.com/CentOS/7/isos/x86_64/CentOS-7-x86_64-NetInstall-1810.iso
# curl -L -O http://mirror.team-cymru.com/CentOS/7/isos/x86_64/sha256sum.txt.asc
# sha256sum -c sha256sum.txt.asc 2>&1 | grep OK
CentOS-7-x86_64-NetInstall-1810.iso: OK
```

#### Step 3: Mount the .iso and copy it's contents to `OEMDRV`
```
#!console
# pwd
/root
# mount -o loop CentOS-7-x86_64-NetInstall-1810.iso mnt
# rsync -av mnt/ OEMDRV/
# umount mnt
```

#### Step 4: Copy your `ks.cfg` file to `OEMDRV`
```
#!console
# pwd
/root
# cp /path/to/your/ks.cfg OEMDRV/ks.cfg
```

#### Step 5: Edit the `isolinux.cfg` file
You'll need to add a stanza to the `isolinux.cfg` file in order to automagically use your `ks.cfg` file. Here's an example:
```
label kickstart
  menu label ^Kickstart
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20x86_64 inst.ks=cdrom:/dev/cdrom:/ks.cfg
```
Note that the label and the menu label are arbitrary text. Note, too, that there's nothing stopping you from adding *multiple stanzas* and utilizing *multiple `ks.cfg` files*. So, if you have 20x servers to kickstart and wish to use 5 different `ks.cfg` files, copy all of them to the directory you use to create the .iso, and reference each of them separately with it's own stanza. Alternatively, you can add all of them to the .iso and hit tab at the boot prompt, and use them directly from there. Perhapsyou can name them by Service Tag, and list them in the `isolinux.cfg` under that name.
```
#!console
# pwd
/root
# cd mnt/isolinux
# vi isolinux.cfg
# cd ../..
```
See [syslinux.org](https://syslinux.org) for a more detailed treatment of this subject.


#### Step 6: Create a new .iso file
```
#!console
# cd OEMDRV
# genisoimage -o ../CentOS-7-x86_64-OEMDRV.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -J -R -v -T -V 'CentOS 7 x86_64' .
# cd ..
# implantisomd5 CentOS-7-x86_64-OEMDRV.iso
```

#### Step 7: Use your new .iso

Now that you've created a new .iso file you can use whatever tools you normally use, such as [Rufus](https://rufus.ie). If created correctly, it will boot to your modified `isolinux.cfg` menu, and boot the system using your `ks.cfg` file.
