#!/bin/bash -xe

# Usage: create-iso.sh [/path/to/ks.cfg] [LABEL]
#
# Requires the following packages be installed:
#  curl, rsync, pykickstart, ksvalidator, genisoimage, isomd5sum

# Make sure you're running as root
if [[ $( id -u ) -ne 0 ]]
then
	echo "You must be root."
	exit 1
fi

# Source the CentOS Image
ISO_URL="http://mirror.team-cymru.com/CentOS/7/isos/x86_64/"
ISO_FILE="CentOS-7-x86_64-NetInstall-1810.iso"
SUM_FILE="sha256sum.txt.asc"
[[ ! -e "$ISO_FILE" ]] && curl -sS -L -O $ISO_URL/$ISO_FILE
[[ ! -e "$SUM_FILE" ]] && curl -sS -L -O $ISO_URL/$SUM_FILE
grep $ISO_FILE $SUM_FILE | sha256sum -c --status -
if [[ $? -ne 0 ]]
then
	echo "Invalid source ISO"
	exit 2
fi

# Validate the Kickstart file
KS="${1:-"ks.cfg"}"
if [[ ! -e "$KS" ]]
then
	echo "ERROR: $KS not found!"
	exit 3
else
	ksvalidator $KS
fi

# Setup a work environment
WORKDIR="$( pwd )"
MOUNT="mnt"
[[ ! -d $MOUNT ]] && mkdir -p $MOUNT
LABEL="${2:-"OEMDRV"}"
[[ ! -d $LABEL ]] && mkdir -p $LABEL || rm -rf $LABEL

# Always remove an existing target ISO
TARGET_ISO="CentOS-7-x86_64-${LABEL}-1810.iso"
[[ -e $TARGET_ISO ]] && rm -f $TARGET_ISO

# Copy the ISO contents to local dir
mount -o loop $ISO_FILE $MOUNT
rsync -av $MOUNT/ $LABEL/
umount $MOUNT

# Copy the kickstart file
cp $KS $LABEL/ks.cfg

# Patch the boot menu with Kickstart option
cd $LABEL/isolinux
patch isolinux.cfg < $WORKDIR/isolinux.cfg.patch
cd $WORKDIR

# Create the new ISO
cd $LABEL
genisoimage -o ../$TARGET_ISO -b isolinux/isolinux.bin -c isolinux/boot.cat \
	-no-emul-boot -boot-load-size 4 -boot-info-table \
	-J -R -v -T -V 'CentOS 7 x86_64' .
cd $WORKDIR
implantisomd5 $TARGET_ISO

# Cleanup
rm -f $ISO_FILE $SUM_FILE
rm -rf $MOUNT $LABEL
