## Kickstart Files

This directory contains a few sample ks.cfg files, hopefully clearly named.

The password assigned to all root users is: `password1234`

There's really no better way to see whats possible in Kickstart files than to delve into the [RHEL 7 Kickstart Syntax Reference](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-kickstart-syntax). A real page turner, I assure you. 
